import React, { Component } from "react";
 
class Contact extends Component {
  render() {
    return (
      <div>
        <h2>GOT QUESTIONS?</h2>
        <p>For having some interaction with me:
        <a href="https://www.linkedin.com/in/santhosh-kumar-m-44410a185/">Linkedin</a>.
        </p>
      </div>
    );
  }
}
 
export default Contact;