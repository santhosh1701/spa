import React, { Component } from "react";
 
class Stuff extends Component {
  render() {
    return (
      <div>
        <h2>STUFF</h2>
        <p>The things you need to know about me:</p>
        <ol>
          <li>DOB:29/07/2000</li>
          <li>Residency Place:Dindigul</li>
          <li>Degree:B.Tech IT</li>
          <li>Interest:Content Creation</li>
        </ol>
      </div>
    );
  }
}
 
export default Stuff;